import io
import os
from Bio.PDB import *
from getEnergyAutomatedPython.logFiles import writeLog

#######################################Alergeno###########################################


def obtenerCadenasFasta(fileName):
    chain = ""
    chainNames = []
    chains = []
    with io.open(fileName,'r') as fastaFile:
        for line in fastaFile:
            #print(line)
            if(line[0]=='>'):        #descripcion de la cadena
                chainNames.append(line[6])
                chains.append(chain[:-1])
                chain=""
            else:
                chain+=line

    chains.append(chain)
    chains.remove("")

    return [chainNames,chains]

def getEpitopes(alergenName,route,chainNames,chains):
    alergenFile = open(route+'Epitopes.txt','w')
    alergenFile.close()
    for (name,chain) in zip(chainNames,chains):
        alergenFile = open(route+'Epitopes.txt','a')
        alergenFile.write("Chain:"+name)
        alergenFile.close()
        curlLine = 'curl --data "method=Bepipred-2.0&sequence_text='+chain+'" http://tools-cluster-interface.iedb.org/tools_api/bcell/ >> '+route+'Epitopes.txt'
        #print(curlLine)
        curlLine.replace('\n','')
        callCurl = os.system(curlLine)

def trimEpitopesFile(route):
    with io.open(route+'Epitopes.txt','r') as fastaFile, io.open(route+'EpitopesTrim.txt','w') as fastaFileTrim:
        bandera = False
        for line in fastaFile:
            if(line[0:5]== "Chain"):
                bandera=True
                fastaFileTrim.write(line)
            elif(line[0:8]=="Position"):
                bandera=False
            elif(bandera):
                fastaFileTrim.write(line)
            #print([line,bandera])



def getStructureEpitopes(route):
    structure = {}
    with io.open(route+"EpitopesTrim.txt",'r') as epitopesFile:
        for line in epitopesFile:
            if(line[0:5]== "Chain"):
                cadena = line[6]
                structure[cadena] = []
            else:
                arreglo = line.split('\t')
                structure[cadena].append([arreglo[1],arreglo[2]])
#    for chain in structure.keys():
#        print("chain: '"+chain+"'")
#        print(structure[chain])
    
    return structure

def getAtoms(igeName,structureABR,selectedChain,selectedABR):       #devuelve los atomos de un solo residuo de la cadena seleccionada
    route = "./procesarAlergeno/PDBS/"
    atoms = []
    structure = PDBParser().get_structure(igeName,route+igeName+".pdb")   #Abre el archivo.

    rangos = structureABR[selectedChain]
    #print (rangos)
    if(len(rangos)>selectedABR):
        rango = rangos[int(selectedABR)]
        #print (rango)
        atoms = []
        for x in range(int(rango[0]),int(rango[1])+1):
            for atom in structure[0][selectedChain][x]:
                atoms.append(atom.get_coord())

    return atoms
    #            print(atom.get_coord())

def obtenerEpitopes(testName,epitope_opc,testPATH):
	#print("Espere mientras realizamos la busqueda de Epitopes.")
	alergenName = "allergen"
	folder = "./procesarAlergeno/fastaGenerated/"
	
	folderEpitopeFiles = "./procesarAlergeno/epitopeFiles/"

	alergenNameFasta = folderEpitopeFiles+alergenName+".fasta"
	route = folder+alergenName+".fasta"
	
	#[chainNames,chains] = obtenerCadenasFasta(alergenNameFasta)  #Del archivo fasta obtengo las cadenas
	#getEpitopes(alergenName,route,chainNames,chains)                #Obtengo los epitopes de cada cadena
	#trimEpitopesFile(route)                             #Corto el archivo para no tener basura

	atomStructureAl = getStructureEpitopes(route)


	#print(atomStructureAl)
	dictCadena = {}
	numCadena = 0;
	for chain in atomStructureAl:
		print("Cadena "+str(numCadena)+" : "+chain+"\n")
		print("Epitopes:"+str(atomStructureAl[chain]))
		dictCadena[numCadena] = chain
		numCadena = numCadena+1
		
	writeLog(testName,"Epitope seleccionada : "+ str(epitope_opc),testPATH)

	atomsEpitope = getAtoms("allergen_autopsf",atomStructureAl,dictCadena[epitope_opc],0)
	return atomsEpitope

