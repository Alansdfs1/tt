import io
import os
from Bio.PDB import *
from getEnergyAutomatedPython.logFiles import writeLog

#######################################Alergeno###########################################

def getStructureParatopos(igeName):
    structure = {}
    with io.open(igeName+"Paratopos.txt",'r') as ParatoposFile:
        #print(ParatoposFile)
        bandera = False
        abrContador = 0
        for line in ParatoposFile:
            #print(len(line))
            if(line[0:8] == "paratome"):
                abrContador = 0
                cadena = line[14]
                #print(cadena)
                structure[cadena] = []
                bandera = True
            elif(bandera and line[0:5]=="Could"):
                bandera = False
            elif(bandera and abrContador<3):
                abrContador = abrContador+1
                arreglo = line.split(' ')
                #print(arreglo)
                rango = arreglo[-1]
                rango = rango[1:-2]
                rango = rango.split(",")
                #print(arreglo[-1])
                #print(rango[0],rango[-1])
                structure[cadena].append([rango[0],rango[-1]])
#    for chain in structure.keys():
#        print("chain: '"+chain+"'")
#        print(structure[chain])
    
    return structure



def getAtoms(igeName,structureABR,selectedChain,selectedABR):       #devuelve los atomos de un solo residuo de la cadena seleccionada
    route = "./procesarAlergeno/PDBS/"
    atoms = []
    structure = PDBParser().get_structure(igeName,route+igeName+".pdb")   #Abre el archivo.

    rangos = structureABR[selectedChain]
    #print(".")
    #print (rangos)
    if(len(rangos)>selectedABR):
        rango = rangos[int(selectedABR)]
        print (rango)
        atoms = []
        for x in range(int(rango[0]),int(rango[1])+1):
            for atom in structure[0][selectedChain][x]:
                atoms.append(atom.get_coord())

    return atoms
    #            print(atom.get_coord())

def obtenerParatopos(testName, paratope_opc, testPATH):
	#print("Espere mientras realizamos la busqueda de Epitopes.")
	igeName = "ige"
	folder = "./procesarIge/paratoposGenerated/"
	
	folderEpitopeFiles = "./procesarIge/paratoposGenerated/"

	alergenNameFasta = folderEpitopeFiles+igeName+".fasta"
	route = folder+igeName
	
	#[chainNames,chains] = obtenerCadenasFasta(alergenNameFasta)  #Del archivo fasta obtengo las cadenas
	#getEpitopes(alergenName,route,chainNames,chains)                #Obtengo los epitopes de cada cadena
	#trimEpitopesFile(route)                             #Corto el archivo para no tener basura

	atomStructureAl = getStructureParatopos(route)


	#print(atomStructureAl)
	dictCadena = {}
	numCadena = 0;
	for chain in atomStructureAl:
		#print("Cadena "+str(numCadena)+" : "+chain)
		#print("Paratopos:"+str(atomStructureAl[chain])+"\n")
		dictCadena[numCadena] = chain
		numCadena = numCadena+1

	writeLog(testName,"Paratopo seleccionado : "+ str(paratope_opc),testPATH)

	atomsEpitope = getAtoms("ige_autopsf",atomStructureAl,dictCadena[paratope_opc],0)
	return atomsEpitope
