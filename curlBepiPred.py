import io
import os
import itertools
from Bio.PDB import *


def obtenerCadenasFasta(fileName):
    chain = ""
    chainNames = []
    chains = []
    with io.open(fileName,'r') as fastaFile:
        for line in fastaFile:
            #print(line)
            if(line[0]=='>'):        #descripcion de la cadena
                chainNames.append(line[6])
                chains.append(chain[:-1])
                chain=""
            else:
                chain+=line

    chains.append(chain)
    chains.remove("")

    return [chainNames,chains]

def getEpitopes(alergenName,chainNames,chains):
    alergenFile = open(alergenName+'Epitopes.txt','w')
    alergenFile.close()
    for (name,chain) in zip(chainNames,chains):
        alergenFile = open(alergenName+'Epitopes.txt','a')
        alergenFile.write("Chain:"+name)
        alergenFile.close()
        curlLine = 'curl --data "method=Bepipred-2.0&sequence_text='+chain+'" http://tools-cluster-interface.iedb.org/tools_api/bcell/ >> '+alergenName+'Epitopes.txt'
        print(curlLine)
        curlLine.replace('\n','')
        callCurl = os.system(curlLine)

def trimEpitopesFile(alergenName):
    with io.open(alergenName+'Epitopes.txt','r') as fastaFile, io.open(alergenName+'EpitopesTrim.txt','w') as fastaFileTrim:
        bandera = False
        for line in fastaFile:
            if(line[0:5]== "Chain"):
                bandera=True
                fastaFileTrim.write(line)
            elif(line[0:8]=="Position"):
                bandera=False
            elif(bandera):
                fastaFileTrim.write(line)
            #print([line,bandera])



def getStructureEpitopes(alergeName):
    structure = {}
    with io.open(alergeName+"EpitopesTrim.txt",'r') as epitopesFile:
        for line in epitopesFile:
            if(line[0:5]== "Chain"):
                cadena = line[6]
                structure[cadena] = []
            else:
                arreglo = line.split('\t')
                structure[cadena].append([arreglo[1],arreglo[2]])
#    for chain in structure.keys():
#        print("chain: '"+chain+"'")
#        print(structure[chain])
    
    return structure

def getAtoms(alergenName,structureAlergenEpitopes,selectedChain):       #devuelve los atomos de un solo residuo de la cadena seleccionada
    atoms = []
    structure = PDBParser().get_structure(alergenName,alergenName+".pdb")	#Abre el archivo.
#    for key in structureAlergenEpitopes:
#        print(key)

    rangos = structureAlergenEpitopes[selectedChain]
    #print (rangos)
    if(len(rangos)>0):
	    rango = rangos[0]
	    #print (rango)
	    atoms = []
	    for x in range(int(rango[0]),int(rango[1])+1):
	        for atom in structure[0][selectedChain][x]:
	            atoms.append(atom.get_coord())

    return atoms
    #            print(atom.get_coord())

alergenName = "1k9u"
alergenNameFasta = alergenName+".fasta"
[chainNames,chains] = obtenerCadenasFasta(alergenNameFasta)  #Del archivo fasta obtengo las cadenas
getEpitopes(alergenName,chainNames,chains)                #Obtengo los epitopes de cada cadena#
trimEpitopesFile(alergenName)                             #Corto el archivo para no tener basura
structure = getStructureEpitopes(alergenName)

#print(structure)

atomsChain = {}

for selectedChain in structure:
#    print (selectedChain)
	atomsChain[selectedChain] = getAtoms(alergenName,structure,selectedChain)

#print("--------------------------")
for chain in atomsChain:
	print(chain)
	atoms = atomsChain[chain]
	print(len(atoms))
	# for atom in atoms:
	#     print(atom)




#print(len(chainNames))
#print(len(chains))

