export const headersStatus = {
    'Procesando':{
        name: 'Procesando',
        color: '#35b4fc'
    },
    'Cancelado':{
        name: 'Cancelado',
        color: '#d65050'
    },
    'Terminado':{
        name: 'Terminado',
        color: 'rgba(139,195,74,1)'
    }
}