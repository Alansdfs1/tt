import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { TableAgComponent } from './table-ag/table-ag.component';
import { RowAgComponent } from './row-ag/row-ag.component';
import { TableMinimaxComponent } from './table-minimax/table-minimax.component';
import { FormAgComponent } from './form-ag/form-ag.component';
import { FormMinimaxComponent } from './form-minimax/form-minimax.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ReplacePipe } from '../pipes/replace.pipe';
import { TooltipModule } from 'ng2-tooltip-directive';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: 'http://69.197.169.74:5000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    SideNavComponent,
    TableAgComponent,
    RowAgComponent,
    TableMinimaxComponent,
    FormAgComponent,
    FormMinimaxComponent,
    ReplacePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TooltipModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
