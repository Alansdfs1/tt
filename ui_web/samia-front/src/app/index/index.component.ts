import { Component, OnInit } from '@angular/core';
import { FormControl} from '@angular/forms';
import { SamiaServiceService } from '../samia-service.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  nuevoAcoplamiento=false;
  public minimaxEjecutions: any[]=[];
  public AGEjecutions: any[]=[];
  constructor(private samiaService: SamiaServiceService) { }
  type = new FormControl('');

  ngOnInit(): void {
    this.type.patchValue('null');
    this.onRefresh();
    this.samiaService
    .getMessage()
    .subscribe(msg => {
      console.log('Incoming msg', msg);
      this.onRefresh();
    });
  }

  onRefresh(){
    this.type.patchValue('null');
    this.samiaService.GetMinimaxEjecutions().subscribe(ejecutions => {this.minimaxEjecutions = ejecutions;console.log(this.minimaxEjecutions)});
    this.samiaService. GetAGEjecutions().subscribe(ejecutions => {this.AGEjecutions = ejecutions;console.log(this.minimaxEjecutions)});
  }



  crearNuevoAcoplamiento(){
    this.nuevoAcoplamiento=true;
  }

  cancelarAcoplamiento(event:boolean){
    this.nuevoAcoplamiento=event;
    this.type.patchValue('null');
  }

}

