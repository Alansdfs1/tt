import { TestBed } from '@angular/core/testing';

import { SamiaServiceService } from './samia-service.service';

describe('SamiaServiceService', () => {
  let service: SamiaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SamiaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
