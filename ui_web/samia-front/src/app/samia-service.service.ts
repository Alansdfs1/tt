import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SamiaServiceService {

  constructor(private http: HttpClient, private socket: Socket) { }

  public GetMinimaxEjecutions():Observable<any>{
    return this.http.get(`${environment.api}/getTasksList/Minmax`)
  }
  public GetAGEjecutions():Observable<any>{
    return this.http.get(`${environment.api}/getTasksList/AlgoritmoGenetico`)
  }
  public CancelEjecution(id:string):Observable<any>{
    return this.http.get(`${environment.api}/revokeTask/${id}`)
  }
  public DeleteEjecution(id:string):Observable<any>{
    return this.http.get(`${environment.api}/dropTask/${id}`)
  }
  public SendEjecutionAGRequest(form:any):Observable<any>{
    return this.http.get(`${environment.api}/createTaskAG/${form.testName}/${form.chainEp}/${form.chainPar}/${form.epocas}/${form.cores}/${form.tipo}/${form.sizeP}`)
  }
  public SendEjecutionMinimaxRequest(form:any):Observable<any>{
    return this.http.get(`${environment.api}/createTaskMinmax/${form.testName}/${form.chainEp}/${form.chainPar}/${form.deep}/0.2/${form.tipo}/${form.coordBegin}/${form.xb}/${form.yb}/${form.zb}`)
  }

  public NameNoExist(name:any):Observable<any>{
    return this.http.get(`${environment.api}/validName/${name}`)
  }
  public getMessage() {
    return this.socket
        .fromEvent("my response").pipe(
          map( data => data )
        );        
  }

  public emitRefresh():Observable<any>{
    return this.http.get(`${environment.api}/testEmit`)
  }

}
