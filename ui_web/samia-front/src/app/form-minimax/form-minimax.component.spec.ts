import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMinimaxComponent } from './form-minimax.component';

describe('FormMinimaxComponent', () => {
  let component: FormMinimaxComponent;
  let fixture: ComponentFixture<FormMinimaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMinimaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMinimaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
