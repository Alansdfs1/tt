import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { SamiaServiceService } from '../samia-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-minimax',
  templateUrl: './form-minimax.component.html',
  styleUrls: ['./form-minimax.component.css']
})

export class FormMinimaxComponent implements OnInit {
  @ViewChild('formDirective') private formDirective: NgForm;
  @Output()
  onCancel = new EventEmitter<boolean>();
  
  nameNoexists = 1;
  
  flagCoordBegin=false;

  constructor(private samiaService: SamiaServiceService) { }

  minimaxForm = new FormGroup({
    testName: new FormControl('', Validators.required),
    deep: new FormControl('', Validators.required),
    chainPar: new FormControl('', Validators.required),
    chainEp: new FormControl('', Validators.required),
    coordBegin: new FormControl('', Validators.required),
    xb: new FormControl(''),
    yb: new FormControl(''),
    zb: new FormControl(''),
    tipo: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    this.minimaxForm.patchValue({
      coordBegin: 'false',
      tipo:'Minmax',
      xb: '0',
      yb: '0',
      zb: '0'
    })
  }
  coordBeginChanges(){
    this.flagCoordBegin=!this.flagCoordBegin;
  }
  onSubmit(){
    if (this.minimaxForm.valid) {
      this.samiaService.SendEjecutionMinimaxRequest(this.minimaxForm.value).subscribe(data=>{
        console.log(data);
        Swal.fire(
          'Enviado',
          'Se ha enviado su petición exitosamente',
          'success'
        ) })
        this.formDirective.resetForm();
    }
  }
  onPropagar(){
    this.onCancel.emit(false);
    this.minimaxForm.patchValue({
      testName: '',
      deep: 'null',
      chainPar: 'null',
      chainEp: 'null',
      coordBegin: 'false',
      xb: 'null',
      yb: 'null',
      zb: 'null'
    })
  }
  onCheckName(e:any){
    var name = this.minimaxForm.controls['testName'].value;
    this.samiaService.NameNoExist(name).subscribe(value => {this.nameNoexists = value.estatus;console.log(this.nameNoexists)});
  }

  get testName() {
    return this.minimaxForm.get('testName');
  }
  get deep() {
    return this.minimaxForm.get('deep');
  }
  get chainPar() {
    return this.minimaxForm.get('chainPar');
  }
  get chainEp() {
    return this.minimaxForm.get('chainEp');
  }
}
