import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableMinimaxComponent } from './table-minimax.component';

describe('TableMinimaxComponent', () => {
  let component: TableMinimaxComponent;
  let fixture: ComponentFixture<TableMinimaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableMinimaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableMinimaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
