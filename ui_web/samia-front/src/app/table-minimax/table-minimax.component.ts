import { Component, OnInit, Input } from '@angular/core';
import {SamiaServiceService} from '../samia-service.service'
import Swal from 'sweetalert2'
import {headersStatus} from '../headersStatus'


@Component({
  selector: 'app-table-minimax',
  templateUrl: './table-minimax.component.html',
  styleUrls: ['./table-minimax.component.css']
})

export class TableMinimaxComponent implements OnInit {
  @Input() minimaxEjecutions: any;
  readonly headersStatus = headersStatus;

  //public minimaxEjecutions: any[]=[];
  

  constructor(private samiaService: SamiaServiceService) { }

  ngOnInit(): void {
    //this.getMinimaxEjecutions();
  }

  //getMinimaxEjecutions(){
  //  this.samiaService.GetMinimaxEjecutions().subscribe(ejecutions => {this.minimaxEjecutions = ejecutions;console.log(this.minimaxEjecutions)});
  //}

  onCancel(id:string){
    this.samiaService.CancelEjecution(id).subscribe(data=>{
      console.log(data);     
      Swal.fire(
        'Cancelado',
        'Se ha cancelado exitosamente',
        'info'
      )})
  }
  onDelete(id:string){
    this.samiaService.DeleteEjecution(id).subscribe(
      data=>{
        console.log(data); 
        Swal.fire(
          'Eliminado',
          'Se ha eliminado exitosamente',
          'info'
        ) })
   
  }
  
}
