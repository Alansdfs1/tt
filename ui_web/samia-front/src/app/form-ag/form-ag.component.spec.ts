import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAgComponent } from './form-ag.component';

describe('FormAgComponent', () => {
  let component: FormAgComponent;
  let fixture: ComponentFixture<FormAgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
