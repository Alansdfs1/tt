import { Component, OnInit, EventEmitter, Output, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { SamiaServiceService } from '../samia-service.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-form-ag',
  templateUrl: './form-ag.component.html',
  styleUrls: ['./form-ag.component.css']
})
export class FormAgComponent implements OnInit {
  @ViewChild('formDirective') private formDirective: NgForm;
  @Output()
  onCancel = new EventEmitter<boolean>();

  nameNoexists = 1;
  constructor(private samiaService: SamiaServiceService) { }
  
  agForm = new FormGroup({
    testName: new FormControl('', Validators.required),
    cores: new FormControl('', Validators.required),
    chainPar: new FormControl('', Validators.required),
    chainEp: new FormControl('', Validators.required),
    sizeP: new FormControl('', Validators.required),
    epocas: new FormControl('', Validators.required),
    tipo: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
    this.agForm.patchValue({
      tipo:'AlgoritmoGenetico'
    })
  }
  onSubmit(){
    if (this.agForm.valid) {
      this.samiaService.SendEjecutionAGRequest(this.agForm.value).subscribe(data=>{
        console.log(data);
        Swal.fire(
          'Enviado',
          'Se ha enviado su petición exitosamente',
          'success'
        ) })
        this.formDirective.resetForm();
        //this.samiaService.emitRefresh();
    }
  }
  onPropagar(){
    this.onCancel.emit(false);
    this.agForm.patchValue({
      testName: '',
      cores: 'null',
      chainPar: 'null',
      chainEp: 'null',
      sizeP: 'null',
      epocas: 'null'
    })
  }
  onCheckName(e:any){
    var name = this.agForm.controls['testName'].value;
    this.samiaService.NameNoExist(name).subscribe(value => {this.nameNoexists = value.estatus;console.log(this.nameNoexists)});
  }
  get testName() {
    return this.agForm.get('testName');
  }
  get cores() {
    return this.agForm.get('cores');
  }
  get chainPar() {
    return this.agForm.get('chainPar');
  }
  get chainEp() {
    return this.agForm.get('chainEp');
  }
  get sizeP() {
    return this.agForm.get('sizeP');
  }
  get epocas() {
    return this.agForm.get('epocas');
  }
}
