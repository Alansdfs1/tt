import { Component, OnInit, Input } from '@angular/core';
import { SamiaServiceService } from '../samia-service.service';
import Swal from 'sweetalert2'
import {headersStatus} from '../headersStatus'


@Component({
  selector: 'app-table-ag',
  templateUrl: './table-ag.component.html',
  styleUrls: ['./table-ag.component.css']
})
export class TableAgComponent implements OnInit {

  readonly headersStatus = headersStatus;
  @Input() AGEjecutions: any;
  //public AGEjecutions: any[]=[];
  
  constructor(private samiaService: SamiaServiceService) { }

  ngOnInit(): void {
    //this.getAGEjecutions();
  }

  //getAGEjecutions(){
  //  this.samiaService. GetAGEjecutions().subscribe(ejecutions => this.AGEjecutions = ejecutions);
  //}

  onCancel(id:string){
    this.samiaService.CancelEjecution(id).subscribe(data=>{
      console.log(data);     
      Swal.fire(
        'Cancelado',
        'Se ha cancelado exitosamente',
        'info'
      )})
  }
  onDelete(id:string){
    this.samiaService.DeleteEjecution(id).subscribe(
      data=>{
        console.log(data); 
        Swal.fire(
          'Eliminado',
          'Se ha eliminado exitosamente',
          'info'
        ) })
   
  }
}
