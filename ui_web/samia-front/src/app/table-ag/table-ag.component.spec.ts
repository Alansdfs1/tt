import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAgComponent } from './table-ag.component';

describe('TableAgComponent', () => {
  let component: TableAgComponent;
  let fixture: ComponentFixture<TableAgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
