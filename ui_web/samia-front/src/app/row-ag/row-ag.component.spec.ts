import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowAgComponent } from './row-ag.component';

describe('RowAgComponent', () => {
  let component: RowAgComponent;
  let fixture: ComponentFixture<RowAgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowAgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowAgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
