import os
import readCoordinates
import readEnergy

def createTCLFIle(idProcess,testName,testPATH):
	route = testPATH+"/"+testName+"/TCLS/"
	newFile = "get_autopsf_coordinates_"+idProcess+".tcl"
	newTCL = route+newFile
	oriTCL = "getEnergyAutomatedPython/TCLS/get_autopsf_coordinates.tcl"

	f_conf = open(oriTCL,"r")
	lines_conf_file = f_conf.readlines()
	f_conf.close()

	for i in range(0, len(lines_conf_file)):
		if(lines_conf_file[i] == "###url\n"):
			lines_conf_file[i] = "cd "+testPATH+"/"+testName+"/merge_files/tmpPDB\n"
		if(lines_conf_file[i] == "###ejecucion\n"):
			lines_conf_file[i] = "mol new "+idProcess+".pdb\n"
					
	archivo = open(newTCL,"w")

	for i in range (0,len(lines_conf_file)):
	    archivo.write(lines_conf_file[i])

	archivo.close()

	return  newFile


def runEnergy(idProcess, testName,testPATH):
	tclFile = createTCLFIle(idProcess, testName,testPATH)		
	coordFile = "coordinates_"+idProcess+".txt"
 
	instrucVmd = "vmd -dispdev text -e "+testPATH+"/"+testName+"/TCLS/"+tclFile+" > "+testPATH+"/"+testName+"/generated_files/"+ coordFile
	#print(instrucVmd)
	os.system(instrucVmd)	
			
	readCoordinates.readCoordinates(idProcess,testName,testPATH)	
	
	#print(datetime.datetime.now())
	confFile = "IgEAllergen_"+idProcess+".conf"
	logFile = "igeAllergenEnergy_"+idProcess+".log"
	instrucNam = "namd2 "+testPATH+"/"+testName+"/CONF/"+confFile+" > "+testPATH+"/"+testName+"/energy_files/"+logFile
	#print(instrucNam)
	os.system(instrucNam)	
	
	energy = readEnergy.readEnergy(idProcess,testName,testPATH)
	#print("Energia: "+energy)

	return energy	
	
