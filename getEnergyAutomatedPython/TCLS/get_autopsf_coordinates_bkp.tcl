package require autopsf
cd getEnergyAutomatedPython
cd merge_files
mol new interPDB.pdb
cd ../generated_files
autopsf -mol 0
set caja [atomselect top all]
measure center $caja
measure minmax $caja
exit
