import re
import numpy as np

def readCoordinates(idProcess,testName,testPATH):
	coordFile = "coordinates_"+idProcess+".txt"
	f =  open(testPATH+"/"+testName+"/generated_files/"+coordFile,'r')

	lines = f.readlines()
	StopWordHeader = 'atomselect'
	center_coordinates= []
	minmax_coordinates = ''
	sum_minmax = []

	for i, line in enumerate(lines):
		if StopWordHeader in line:
			center_line = i+1
			center_coordinates = lines[center_line]
			minmax_coordinates = lines[center_line+1]
			break

	center_coordinates = center_coordinates.split()
	center_coordinates = np.array(center_coordinates).astype(np.float)

	minmax_coordinates = minmax_coordinates.replace('} {',';')

	min_coordinates = minmax_coordinates.split(';')[:1]
	min_coordinates = ' '.join(map(str, min_coordinates))
	min_coordinates = min_coordinates.replace('{','')
	min_coordinates = min_coordinates.split()
	min_coordinates = abs(np.array(min_coordinates).astype(np.float))

	max_coordinates = minmax_coordinates.split(';')[1:]
	max_coordinates = ' '.join(map(str, max_coordinates))
	max_coordinates = max_coordinates.replace('}','')
	max_coordinates = max_coordinates.split()
	max_coordinates = abs(np.array(max_coordinates).astype(np.float))

	for i in range(3):
		sum_minmax.append((min_coordinates[i] + max_coordinates[i])+ 40)

	#print 'Center coordinates: '+ str(center_coordinates)
	#print 'Minimos y maximos: '+ str(sum_minmax)

	f_conf = open("getEnergyAutomatedPython/CONF/IgEAllergen.conf","r")#Config general
	lines_conf_file = f_conf.readlines()

	for i in range(0, len(lines_conf_file)):
		if(lines_conf_file[i] == "# Periodic Boundary Conditions\n"):
			lines_conf_file[i+1] = "cellBasisVector1"+4*" "+str(sum_minmax[0])+4*" "+"0.0"+4*" "+"0.0\n"
			lines_conf_file[i+2] = "cellBasisVector2"+4*" "+"0.0"+4*" "+str(sum_minmax[1])+4*" "+"0.0\n"
			lines_conf_file[i+3] = "cellBasisVector3"+4*" "+"0.0"+4*" "+"0.0"+4*" "+str(sum_minmax[2])+"\n"
			lines_conf_file[i+4] = "cellOrigin"+10*" "+str(center_coordinates[0])+" "+str(center_coordinates[1])+" "+str(center_coordinates[2])+"\n"
		if(lines_conf_file[i] == "###structure\n"):
			lines_conf_file[i] = "structure          ../generated_files/"+idProcess+"_autopsf.psf\n"
		if(lines_conf_file[i] == "###coordinates\n"):
			lines_conf_file[i] = "coordinates        ../generated_files/"+idProcess+"_autopsf.pdb\n"
		if(lines_conf_file[i] == "###output\n"):
			lines_conf_file[i] = "set outputname     ../energy_files/ige_allergen_"+idProcess+"\n"


	confFile = "IgEAllergen_"+idProcess+".conf"
	archivo = open(testPATH+"/"+testName+"/CONF/"+confFile,"w")

	for i in range (0,len(lines_conf_file)):
	    archivo.write(lines_conf_file[i])

