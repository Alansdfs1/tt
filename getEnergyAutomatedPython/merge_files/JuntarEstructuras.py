from Bio.PDB import *
import numpy

#-------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------Funciones-----------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------#
def rotarX(structure,angle):
	radianes = (2*3.14159*angle)/360# La funciontransform trabaja en radianes
	rotation_matrix = rotaxis2m(radianes, Vector(0,1,0))# Gira a la derecha
	translation_matrix = numpy.array((0, 0, 0), 'f')# No lo roto pero la funcion transform lo pide.

	for atom in structure.get_atoms():#Recorre los atomos de la estructura
	     atom.transform(rotation_matrix, translation_matrix)#Aplica la rotacion
	return structure

def trasladar(structure,x,y,z):# La funcion transform trabaja en radianes
	rotation_matrix = rotaxis2m(0, Vector(0,0,0))# No lo giro pero la funcion transform lo pide.
	translation_matrix = numpy.array((x,y,z), 'f')# Se traslada.

	for atom in structure.get_atoms():#Recorre los atomos de la estructura
	     atom.transform(rotation_matrix, translation_matrix)#Aplica la traslacion
	return structure

def obtenerCentro(structure):
	x_cMax = -999999
	y_cMax = -999999
	z_cMax = -999999
	x_cMin = 999999
	y_cMin = 999999
	z_cMin = 999999


	for atom in structure.get_atoms():#Recorre atomos y suma posiciones en variables
		v = atom.get_vector()
		if(v[0]>x_cMax):
			x_cMax = v[0]
		if(v[0]<x_cMin):
			x_cMin = v[0]

		if(v[1]>y_cMax):
			y_cMax = v[1]
		if(v[1]<y_cMin):
			y_cMin = v[1]

		if(v[2]>z_cMax):
			z_cMax = v[2]
		if(v[2]<z_cMin):
			z_cMin = v[2]

	x_c = (x_cMax+x_cMin)/2
	y_c = (y_cMax+y_cMin)/2
	z_c = (z_cMax+z_cMin)/2



	return [x_c,y_c,z_c]	#Retorna los promedios.


#-------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------Main--------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------#
def interPDB(igeName,alergenName,c1,c2,c3,name,testName,testPATH):
	route = "./getEnergyAutomatedPython/merge_files/"
	routeTest = "./"+testPATH+"/"+testName+"/merge_files/"

	structure = PDBParser().get_structure(igeName, route + "PDBS/" + igeName+".pdb")#Abre el archivo.
	# print(structure)
	[x_cIge,y_cIge,z_cIge] = obtenerCentro(structure)
	#print([x_cIge,y_cIge,z_cIge])
	structure = trasladar(structure,-x_cIge,-y_cIge,-z_cIge)#Traslado la estructura al centro(0,0,0)
	#print(obtenerCentro(structure))

	structure2 = PDBParser().get_structure(alergenName, route + 'PDBS/' + alergenName+".pdb")#Abre el archivo.
	# print(structure2)
	[x_cAl,y_cAl,z_cAl] = obtenerCentro(structure2)
	#print([x_cIge,y_cIge,z_cIge])
	structure2 = trasladar(structure2,-x_cAl,-y_cAl,-z_cAl)							#Traslado la estructura casi al centro 
	#print("centros")
	#print(obtenerCentro(structure2))
	desplazamiento = list()

	#for m in range(0,3):
	#	print("ingresa la coordenada "+str(m))
	#	n = int(input())
	#	desplazamiento.append(n)

	desplazamiento.append(c1)
	desplazamiento.append(c2)
	desplazamiento.append(c3)
	##print(desplazamiento)

	structure2 = trasladar(structure2,desplazamiento[0],desplazamiento[1],desplazamiento[2])#Traslado la estructura casi al centro 
	#print(obtenerCentro(structure2))

	chains = list(structure2.get_chains())# Obtiene las cadenas de la estructura.
	## print(chains)

	nombresChain = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	cadenasRecorridas = {}
	m = 15
	for chain in chains:# Por cada cadena en la estructura
		if(chain.id not in cadenasRecorridas):
			cadenasRecorridas[chain.id] = nombresChain[m]
			chain.id = nombresChain[m]		# Renombra la cadena, no pueden haber 2 cadenas con el mismo nombre
			m = m+1
			#print(m)
		else:
			chain.id = cadenasRecorridas[chain.id]

	io = PDBIO()			#Para crear el archivo
	io.set_structure(structure)	#Agrega la estructura
	io.save(routeTest+ "tmpPDB/" + igeName+'T_'+name+'.pdb')	#Guarda.
	io = PDBIO()
	io.set_structure(structure2)	#Agrega la estructura
	io.save(routeTest + "tmpPDB/" + alergenName+'T_'+name+'.pdb')		#Guarda.


	with open(routeTest + "tmpPDB/" + name+'.pdb', 'w') as outfile:
		#print(routeTest + "tmpPDB/" + name+'.pdb')

		with open(routeTest + "tmpPDB/" + igeName+'T_'+name+'.pdb') as infile: 
			lines = infile.readlines()
			outfile.writelines([item for item in lines[:-1]]) 
		with open(routeTest + "tmpPDB/" + alergenName+'T_'+name+'.pdb') as infile: 
			outfile.write(infile.read())  

