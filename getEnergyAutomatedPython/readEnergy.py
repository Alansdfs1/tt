import re
import pandas as pd

def readEnergy(idProcess,testName,testPATH):
	logFile = "igeAllergenEnergy_"+idProcess+".log"
	f =  open(testPATH+"/"+testName+"/energy_files/"+logFile,'r')

	lines = f.readlines()
	StopWordHeader = 'ETITLE'
	StopWordEnergy = '^ENERGY:'
	StopWordError = 'ERROR:'
	header = 0
	error = 0

	parameters = None
	energyValues = []

	for line in lines:
		if header == 0 and StopWordHeader in line:
			parameters = line.split()[1:]
			header = 1 
			#print parameters
		if re.match(StopWordEnergy, line) is not None:		
			values = line.split()[1:]
			energyValues.append(values)
			#print energyValues
		if re.match(StopWordError, line) is not None:
			error = 1

	df = pd.DataFrame(energyValues,columns=parameters)

	if error == 1:
		df = str(9999999)
	else:
		df = df.iloc[-1,12]

	return df
