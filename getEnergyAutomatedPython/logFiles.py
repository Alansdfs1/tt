import requests
import json

def updateEpoca(testName,epoca):
    req = requests.get('http://69.197.169.74:3000/tasks?nombre='+testName+"&_limit=1")  
    itemList = json.loads(req.text)
    item = itemList[0] 
    id = item.get('id')        
    item['epoca']=str(epoca)        
    req = requests.put('http://69.197.169.74:3000/tasks/'+id, data = item)
    sendUploadFlag()

def updateAGScores(testName,scoreDistancia,scoreEnergia,scoreTotal,mejor):
    req = requests.get('http://69.197.169.74:3000/tasks?nombre='+testName+"&_limit=1")  
    itemList = json.loads(req.text)    
    
    if(len(itemList)>0):  
      item = itemList[0]      
      id = item.get('id')        
      item['mejor']=str(mejor)
      item['score_energia']=str(scoreEnergia)
      item['score_distancia']=str(scoreDistancia)
      item['score_total']=str(scoreTotal)   
       
      req = requests.put('http://69.197.169.74:3000/tasks/'+id, data = item)
      sendUploadFlag()


def sendUploadFlag():
    req = requests.get('http://69.197.169.74:3000/update/1')  
    item = json.loads(req.text)          
    item['avalaible']="1"
    req = requests.put('http://69.197.169.74:3000/update/1', data = item)    

def updateMinmaxScores(testName,scoreDistancia,scoreEnergia,scoreTotal,mejor):
    print('http://69.197.169.74:3000/tasks?nombre='+testName+'&_limit=1')
    req = requests.get('http://69.197.169.74:3000/tasks?nombre='+testName+'&_limit=1')  
    itemList = json.loads(req.text)
    item = itemList[0]
    id = item.get('id')    
    
    if(float(item.get('score_total')) > float(scoreTotal) ):   
        item['mejor']=str(mejor)
        item['score_energia']=str(scoreEnergia)
        item['score_distancia']=str(scoreDistancia)
        item['score_total']=str(scoreTotal)   
     
        req = requests.put('http://69.197.169.74:3000/tasks/'+id, data = item)
        
def finishTask(testName):
    req = requests.get('http://69.197.169.74:3000/tasks?nombre='+testName+"&_limit=1")  
    itemList = json.loads(req.text)
    item = itemList[0]
    id = item.get('id')        
    item['estatus']="Terminado" 
     
    req = requests.put('http://69.197.169.74:3000/tasks/'+id, data = item)
    sendUploadFlag()

def writeLog(testName, mensage,testPATH):
    f = open (testPATH+"/"+testName+"/logsEjecucion.txt",'a+')
    f.write(mensage + '\n' )
    f.close()