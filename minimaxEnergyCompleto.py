from getEnergyAutomatedPython import runEnergy
from getEnergyAutomatedPython.merge_files import JuntarEstructuras
from procesarIge import obtenerParatopos
from procesarAlergeno import obtenerEpitopes
from getEnergyAutomatedPython.logFiles import writeLog,updateEpoca,updateMinmaxScores,finishTask

from Bio.PDB import *
import random
import math
import numpy
import datetime
import os
import shutil
from argparse import ArgumentParser

def crearDirectorios(testName,testPath):
    path = testPath+"/"+testName
    if os.path.isdir(path):
        shutil.rmtree(path)
    
    os.makedirs(path+"/CONF")
    os.makedirs(path+"/energy_files")
    os.makedirs(path+"/generated_files")
    os.makedirs(path+"/merge_files/tmpPDB")
    os.makedirs(path+"/TCLS")

def recorrerAtoms(atoms,x,y,z):
    recorrido = []
    for atom in atoms:
        recorrido.append(atom + [x,y,z])
    return recorrido

def distanciaPuntos3D(atomAl,atomIge):
    return math.sqrt(math.pow(float(atomAl[0])-float(atomIge[0]),2)+math.pow(float(atomAl[1])-float(atomIge[1]),2)+math.pow(float(atomAl[2])-float(atomIge[2]),2))
    
'''def funcionEvaluacion(atomsAlergenSelected,atomsIgeSelected):
    distanciaMax = 0
    for atomAl in atomsAlergenSelected:
        for atomIge in atomsIgeSelected:
            distancia = distancia + distanciaPuntos3D(atomAl,atomIge)
    return distancia
    '''
def funcionEvaluacion(atomsAlergenSelected,atomsIgeSelected):
    distancia = 0
    minimo = 99999999
    for atomAl in atomsAlergenSelected:
        for atomIge in atomsIgeSelected:
            distancia = distanciaPuntos3D(atomAl,atomIge)
            if(distancia < minimo):
            	minimo=distancia
    return minimo

def obtenerCentro(structure):
    distanciaMax = 0
    x_cMax = -999999
    y_cMax = -999999
    z_cMax = -999999
    x_cMin = 999999
    y_cMin = 999999
    z_cMin = 999999


    for atom in structure.get_atoms():#Recorre atomos y suma posiciones en variables
        v = atom.get_vector()
        if(v[0]>x_cMax):
            x_cMax = v[0]
        if(v[0]<x_cMin):
            x_cMin = v[0]

        if(v[1]>y_cMax):
            y_cMax = v[1]
        if(v[1]<y_cMin):
            y_cMin = v[1]

        if(v[2]>z_cMax):
            z_cMax = v[2]
        if(v[2]<z_cMin):
            z_cMin = v[2]

    x_c = (x_cMax+x_cMin)/2
    y_c = (y_cMax+y_cMin)/2
    z_c = (z_cMax+z_cMin)/2

    for atom in structure.get_atoms():  #Recorre atomos y suma posiciones en variables
        v = atom.get_vector()
        ide = atom.get_serial_number()
        dis = abs(x_c - v[0])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerX: "+str(x_c)+" actualX: "+str(v[0])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))

        dis = abs(y_c - v[1])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerY: "+str(y_c)+" actualX: "+str(v[1])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))


        dis = abs(z_c - v[2])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerZ: "+str(z_c)+" actualX: "+str(v[2])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))


    return x_c,y_c,z_c,distanciaMax,pos  #Retorna los promedios.

def randomPoint(radio):
    mean = 0
    standard_deviation = 10
    rand  = numpy.random.normal(mean, standard_deviation)
    rand2  = numpy.random.normal(mean, standard_deviation)
    rand3  = numpy.random.normal(mean, standard_deviation)
    norm = math.sqrt(math.pow(rand,2) + math.pow(rand2,2) + math.pow(rand3,2))
    xrand = rand*radio/norm
    yrand = rand2*radio/norm
    zrand = rand3*radio/norm
    return [xrand,yrand,zrand]

def modificarCoordenada(inicio,movimiento):
    return [inicio[0]+movimiento[0],inicio[1]+movimiento[1],inicio[2]+movimiento[2]]

def minimax(inicio,profundidad,jugador,profundidadActual,igeName,allergenName):
    
    if(profundidad==profundidadActual):
        #print("profundidad")
        #print(inicio)
        if(str(inicio) not in recorridosTotal.keys()):
            #print("coordenada: "+str(inicio))
            writeLog(nameTest,"coordenada: "+str(inicio),testPath)

            auxAllergen = recorrerAtoms(allergenAtoms,inicio[0],inicio[1],inicio[2])
            distancia = funcionEvaluacion(igeAtoms,auxAllergen)
            distancia = distancia*1000
            
            process = "M"+str(inicio[0]).replace('.','_')+"-"+str(inicio[1]).replace('.','_')+"-"+str(inicio[2]).replace('.','_')
            JuntarEstructuras.interPDB(igeName,allergenName,inicio[0],inicio[1],inicio[2],process,nameTest,testPath)  
            d = float(runEnergy.runEnergy(process,nameTest,testPath))            
            total = d+distancia

            #print("distancia : "+str(distancia)+"  energia: "+str(d)+" = "+str(total))
            writeLog(nameTest,"distancia : "+str(distancia)+"  energia: "+str(d)+" = "+str(total),testPath)                                

            recorridosTotal[str(inicio)] = total
            recorridosEnergia[str(inicio)] = d
            recorridosDistancia[str(inicio)] = distancia 
        else:
            total = recorridosTotal[str(inicio)]
        #print(str(inicio)+" : "+str(total))
        writeLog(nameTest,str(inicio)+" : "+str(total),testPath)

        return [total,None] #eval random

    if(jugador == "MAX"):
        mejor = [9999999,None]
        for movimiento in posiblesMovimientos:
            nuevo = modificarCoordenada(inicio,movimiento)
            aux = minimax(nuevo,profundidad,"MIN",profundidadActual+1,igeName,allergenName)
            if(aux[0] < mejor[0]):
                mejor = [aux[0],movimiento]
    else:
        mejor = [-9999999,None]
        for movimiento in posiblesMovimientos:
            nuevo = modificarCoordenada(inicio,movimiento)
            aux = minimax(nuevo,profundidad,"MAX",profundidadActual+1,igeName,allergenName)
            if(aux[0] > mejor[0]):
                mejor = [aux[0],movimiento]

    return mejor

##Main

recorridosTotal = {}
recorridosEnergia = {}
recorridosDistancia = {}

allergenName = "allergen_autopsf"
igeName = "ige_autopsf"
route = "./getEnergyAutomatedPython/merge_files/"

parser = ArgumentParser()
parser.add_argument("-n", "--name", dest="test_name", help="nombre del directorio de prueba", metavar="DIRECTORIO")
parser.add_argument("-e", "--epitope", dest="epitope_opc", help="configuracion de epitope", metavar="OPCION",default =0)
parser.add_argument("-p", "--paratope", dest="paratope_opc", help="configuracion de paratope", metavar="OPCION", default =1)
parser.add_argument("-xb", "--x", dest="x_begin", help="coordenada x de punto de inicio", metavar="NUMERO", default = 0)
parser.add_argument("-yb", "--Y", dest="y_begin", help="coordenada y de punto de inicio", metavar="NUMERO", default = 0)
parser.add_argument("-zb", "--z", dest="z_begin", help="coordenada z de punto de inicio", metavar="NUMERO", default = 0)
parser.add_argument("-d", "--deep", dest="deep", help="profundidad del algoritmo minimax", metavar="NUMERO", default = 1)
parser.add_argument("-s", "--step", dest="step_opc", help="tam. de paso", metavar="NUMERO", default =0.2)
parser.add_argument("-rb", "--randombegin",action="store_false", dest="random_begin", default=True, help="punto de inicio random")

args = parser.parse_args()

#print(args)

nameTest = str(args.test_name)
epitope_opc = int(args.epitope_opc)
paratope_opc = int(args.paratope_opc)
x_begin = float(args.x_begin)
y_begin = float(args.y_begin)
z_begin = float(args.z_begin)
deep = int(args.deep)
step=float(args.step_opc)
random_begin = int(args.random_begin)

testPath = "ResultsMinMax"

if(nameTest == "error"):
    print("Debe ingresar un nombre de test")
    exit(0)

posiblesMovimientos = [[step,0,0],[0,step,0],[0,0,step],[-step,0,0],[0,-step,0],[0,0,-step]]
#posiblesMovimientos = [[1,0,0],[0,1,0]]

#print("Ingrese el nombre de la prueba :")
#nameTest = str(raw_input())

crearDirectorios(nameTest,testPath)

writeLog(nameTest,"Directorio prueba : "+ str(nameTest),testPath)

structure = PDBParser().get_structure(igeName, route + "PDBS/" +igeName+".pdb")  #Abre el archivo.
[x_cIge,y_cIge,z_cIge,distanciaMaxIge,posIge] = obtenerCentro(structure)

structure2 = PDBParser().get_structure(allergenName, route + "PDBS/" + allergenName+".pdb") #Abre el archivo.
[x_cAl,y_cAl,z_cAl,distanciaMaxAl,posAl] = obtenerCentro(structure2)

#print([distanciaMaxAl,posAl,distanciaMaxIge,posIge])

igeAtoms = obtenerParatopos.obtenerParatopos(nameTest,paratope_opc,testPath)
igeAtoms = recorrerAtoms(igeAtoms,-x_cIge,-y_cIge,-z_cIge)
#print(len(igeAtoms))

allergenAtoms = obtenerEpitopes.obtenerEpitopes(nameTest,epitope_opc,testPath)
allergenAtoms = recorrerAtoms(allergenAtoms,-x_cAl,-y_cAl,-z_cAl)
#print(len(allergenAtoms))


radio = (distanciaMaxAl+distanciaMaxIge)*1.5

writeLog(nameTest,"Tam. Radio : "+str(radio),testPath)


profundidad = deep
writeLog(nameTest,"Profundidad : "+str(deep),testPath)
if(random_begin):
    inicio = [x_begin,y_begin,z_begin]
else:
    inicio = randomPoint(radio)
writeLog(nameTest,"Coordenadas de inicio : "+str(inicio),testPath) 
#inicio = [55.20,9.80,-1.80]
#print('Inicio general')
#print(datetime.datetime.now())

writeLog(nameTest,"Inicio general",testPath)
writeLog(nameTest,str(datetime.datetime.now()),testPath)

for m in range(0,50):
    #print('Inicio ciclo')
    writeLog(nameTest,"Inicio ciclo",testPath)
    updateEpoca(nameTest,str(m+1))

    #print(datetime.datetime.now())
    resultado = minimax(inicio,profundidad,"MAX",0,igeName,allergenName)
    #print(resultado)
    inicio = modificarCoordenada(inicio,resultado[1])
    JuntarEstructuras.interPDB(igeName,allergenName,inicio[0],inicio[1],inicio[2],"minimaxInter"+str(m),nameTest,testPath)
    #print("\n\n"+str(m)+"--------------------------------------------------------------------------------------")    
    #print("minimaxInter"+str(m))

    #print(resultado)
    #print(inicio)
    #print('Fin ciclo')

    writeLog(nameTest,"\n\n"+str(m)+"--------------------------------------------------------------------------------------",testPath)
    writeLog(nameTest,"Restultado: "+str(inicio),testPath)
    writeLog(nameTest,"Paso : "+str(m)+" Coordenada : "+str(inicio)+"distancia : "+str(recorridosDistancia[str(inicio)])+" energia: "+str(recorridosEnergia[str(inicio)])+" = "+str(recorridosTotal[str(inicio)]),testPath)
    writeLog(nameTest,"minimaxInter"+str(m),testPath)
    writeLog(nameTest,"Nuevo: "+str(inicio),testPath)
    writeLog(nameTest,"Fin ciclo",testPath)
    updateMinmaxScores(nameTest,str(recorridosDistancia[str(inicio)]),str(recorridosEnergia[str(inicio)]),str(recorridosTotal[str(inicio)]),str(inicio))   

writeLog(nameTest,"Fin general",testPath)
writeLog(nameTest,str(datetime.datetime.now()),testPath)
finishTask(nameTest)


filenameBest = "bestAcoplamiento"+nameTest
JuntarEstructuras.interPDB(igeName,allergenName,float(inicio[0]),float(inicio[1]),float(inicio[2]),filenameBest,nameTest,testPath)

print('Final general')
print(datetime.datetime.now())
