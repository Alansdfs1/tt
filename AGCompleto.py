from getEnergyAutomatedPython import runEnergy
from getEnergyAutomatedPython.merge_files import JuntarEstructuras
from procesarIge import obtenerParatopos
from procesarAlergeno import obtenerEpitopes
from getEnergyAutomatedPython.logFiles import writeLog,updateEpoca,updateAGScores,finishTask
from Bio.PDB import *
import random
import math
import numpy
import concurrent.futures
import datetime
import os
import shutil
from argparse import ArgumentParser


def crearDirectorios(testName,testPATH):
    path = testPATH+"/"+testName
    if os.path.isdir(path):
        shutil.rmtree(path)
    
    os.makedirs(path+"/CONF")
    os.makedirs(path+"/energy_files")
    os.makedirs(path+"/generated_files")
    os.makedirs(path+"/merge_files/tmpPDB")
    os.makedirs(path+"/TCLS")

def recorrerAtoms(atoms,x,y,z):
	recorrido = []
	for atom in atoms:
		recorrido.append(atom + [x,y,z])
	return recorrido

def distanciaPuntos3D(atomAl,atomIge):
    return math.sqrt(math.pow(float(atomAl[0])-float(atomIge[0]),2)+math.pow(float(atomAl[1])-float(atomIge[1]),2)+math.pow(float(atomAl[2])-float(atomIge[2]),2))

# def funcionEvaluacion(atomsAlergenSelected,atomsIgeSelected):
#     distancia = 0
#     for atomAl in atomsAlergenSelected:
#         for atomIge in atomsIgeSelected:
#             distancia = distancia + distanciaPuntos3D(atomAl,atomIge)
#     return distancia

def funcionEvaluacion(atomsAlergenSelected,atomsIgeSelected):
    distancia = 0
    minimo = 99999999
    for atomAl in atomsAlergenSelected:
        for atomIge in atomsIgeSelected:
            distancia = distanciaPuntos3D(atomAl,atomIge)
            if(distancia < minimo):
            	minimo=distancia
    return minimo

def obtenerCentro(structure):
    distanciaMax = 0
    x_cMax = -999999
    y_cMax = -999999
    z_cMax = -999999
    x_cMin = 999999
    y_cMin = 999999
    z_cMin = 999999


    for atom in structure.get_atoms():#Recorre atomos y suma posiciones en variables
        v = atom.get_vector()
        if(v[0]>x_cMax):
            x_cMax = v[0]
        if(v[0]<x_cMin):
            x_cMin = v[0]

        if(v[1]>y_cMax):
            y_cMax = v[1]
        if(v[1]<y_cMin):
            y_cMin = v[1]

        if(v[2]>z_cMax):
            z_cMax = v[2]
        if(v[2]<z_cMin):
            z_cMin = v[2]

    x_c = (x_cMax+x_cMin)/2
    y_c = (y_cMax+y_cMin)/2
    z_c = (z_cMax+z_cMin)/2

    for atom in structure.get_atoms():  #Recorre atomos y suma posiciones en variables
        v = atom.get_vector()
        ide = atom.get_serial_number()
        dis = abs(x_c - v[0])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerX: "+str(x_c)+" actualX: "+str(v[0])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))

        dis = abs(y_c - v[1])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerY: "+str(y_c)+" actualX: "+str(v[1])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))


        dis = abs(z_c - v[2])
        if(dis > distanciaMax):
            distanciaMax = dis
            pos = ide
        #print("resta: centerZ: "+str(z_c)+" actualX: "+str(v[2])+" posX: "+str(ide)+" dis: "+str(dis)+" distanciaMaxActual: "+str(distanciaMax))


    return x_c,y_c,z_c,distanciaMax,pos  #Retorna los promedios.

def randomPoint(radio):
	mean = 0
	standard_deviation = 10
	rand  = numpy.random.normal(mean, standard_deviation)
	rand2  = numpy.random.normal(mean, standard_deviation)
	rand3  = numpy.random.normal(mean, standard_deviation)
	norm = math.sqrt(math.pow(rand,2) + math.pow(rand2,2) + math.pow(rand3,2))
	xrand = rand*radio/norm
	yrand = rand2*radio/norm
	zrand = rand3*radio/norm
	return [xrand,yrand,zrand]

def inicializarPoblacion(tam,radio):
	poblacion = []
	for x in range(0,tam):
		poblacion.append(randomPoint(radio))
	return poblacion

#########################################################
##         la seleccion sera por torneo 1 vs 1         ##
##         cada ganador tendra derecho a vivir         ##
##            de los padres 1 o 2 mutaran              ##
def seleccionTorneo(poblacion1,atomsAlergenSelected,atomsIgeSelected,igeName,alergenName):
	poblacionDominante = []
	poblacion = poblacion1[:]
	while(len(poblacion)>0):
		randm = int(math.floor(random.random()*len(poblacion)))
		sel1 = poblacion.pop(randm)
		if(len(poblacion)>0):
			randm2 = int(math.floor(random.random()*len(poblacion)))
			sel2 = poblacion.pop(randm2)

			igeModificado = {}

			#igeModificado  = recorrerAtoms(atomsIgeSelected,int(sel1[0]),int(sel1[1]),int(sel1[2]))

			#eval1 = funcionEvaluacion(atomsAlergenSelected,igeModificado)
			key = str(sel1[0])+str(sel1[1])+str(sel1[2])

			eval1 = evaluados[key]
			igeModificado = {}

			#igeModificado  = recorrerAtoms(atomsIgeSelected,int(sel2[0]),int(sel2[1]),int(sel2[2]))

			#eval2 = funcionEvaluacion(atomsAlergenSelected,igeModificado)
			key = str(sel2[0])+str(sel2[1])+str(sel2[2])

			eval2 = evaluados[key]
			if(eval1 < eval2): #se escoje al que tiene menor distancia
				poblacionDominante.append(sel1)
			    #poblacionDominante.append(mutacion(sel2))
			else:
				poblacionDominante.append(sel2)
			    #poblacionDominante.append(mutacion(sel2))
		else:
			poblacionDominante.append(sel1)
	return poblacionDominante

#########################################################
##    La cruza sera una media entre puntos z1+z2/2     ##

def cruza(punto1,punto2):
	direccion = int(math.floor(random.random()*2))
	if(direccion==0):
		multi = 0.8
	else:
		multi = 1.2
	

	##print(nCromosoma)
	hijo = punto1[:]
	hijo[0] += punto2[0]
	hijo[0] = (hijo[0]/2)*multi

	hijo[1] += punto2[1]
	hijo[1] = (hijo[1]/2)*multi

	hijo[2] += punto2[2]
	hijo[2] = (hijo[2]/2)*multi

	
	return hijo


def cruzaPoblacion(poblacion):
	poblacionCruzada = []
	padres = poblacion[:]
	while(len(padres)>0):
		randm = int(math.floor(random.random()*len(padres)))
		sel1 = padres.pop(randm)
		if(len(padres)>0):
			randm2 = int(math.floor(random.random()*len(padres)))
			sel2 = padres.pop(randm2)
			poblacionCruzada.append(cruza(sel1,sel2))
		else:
			poblacionCruzada.append(sel1)
	padres = poblacion[:]

	while(len(padres)>0):
		poblacionCruzada.append(mutacion(padres.pop()))

	while(len(poblacionCruzada)<tamPoblacion):
		randm = int(math.floor(random.random()*len(poblacion)))
		sel1 = poblacion.pop(randm)
		poblacionCruzada.append(sel1)

	return poblacionCruzada

#########################################################
##   el mejor pudo haber sido intercambiado pero no    ##
##   queremos que desaparezca por lo cual lo volvemos  ##
##                     a insertar                      ##

def insertarMejor(poblacion,mejor):
	randm = int(math.floor(random.random()*len(poblacion)))
	poblacion.pop(randm)
	poblacion.append(mejor)
	return poblacion
#########################################################
##   La mutacion sera modificar una coordenada x o y   ##
##           entre los padres que no mutaron           ##
def mutacion(punto1):

	nCromosoma = int(math.floor(random.random()*3))
	hijo = punto1[:]
	hijo[nCromosoma] = hijo[nCromosoma]*0.80
	return hijo

def getValorEvaluacion(params):
	process = "interPDB_"+ str(params[5])
	JuntarEstructuras.interPDB(params[0],params[1],params[2],params[3],params[4],process,params[6],params[7])
	eval1 = runEnergy.runEnergy(process,params[6], params[7])
	#eval1 = 0
	return eval1

def evaluacionPoblacion(poblacion,allergenAtoms,igeAtoms,igeName,alergenName,numEpoca,nameTest,testPATH):
	global mejorGlobal
	global TotalGlobal
	global EnergiaGlobal
	global DistanciaGlobal
	poblacionEvaluar = poblacion[:]
	#print("Tamano poblacion: "+str(len(poblacion)))
	evalPoblacion = 0
	mejor = 1000000
	inidividuoMejor = None	

	listParamsPoblacion = []
	
	idProcess = 100	

	while(len(poblacionEvaluar)>0):
		aux = poblacionEvaluar.pop()
		key = str(aux[0])+str(aux[1])+str(aux[2])
		#print(key)

		if(key in evaluados):
			eval1 = evaluados[key]
			if(eval1<mejor):
				mejor = eval1
				inidividuoMejor = [aux[0],aux[1],aux[2]]
		else:		
			idProcess = idProcess + 1 
			idGeneral = str(idProcess) + "_epoca"+str(numEpoca)
			params = [igeName,allergenName,aux[0],aux[1],aux[2],idGeneral,nameTest,testPATH]
			listParamsPoblacion.append(params)

	#print("#### Inicia paralelismo ####")
	#print(len(listParamsPoblacion))

	with concurrent.futures.ThreadPoolExecutor(max_workers=num_cores) as executor:
		for params, result in zip(listParamsPoblacion,executor.map(getValorEvaluacion,listParamsPoblacion)):
			#print('%s resultado de la evaluacion %s'% (params,result))
			writeLog(nameTest,'%s resultado de la evaluacion %s'% (params,result),testPATH)

			auxAllergen = recorrerAtoms(allergenAtoms,params[2],params[3],params[4])
			distancia = funcionEvaluacion(igeAtoms,auxAllergen)
			##distancia = distancia * 500/(len(igeAtoms)*len(auxAllergen))
			distancia = distancia * 1000
			total = float(result)+distancia

			#print("Distancia "+str(distancia)+" Energia: "+str(float(result))+" total: "+str(total))
			writeLog(nameTest,"Distancia "+str(distancia)+" Energia: "+str(float(result))+" total: "+str(total),testPATH)

			evalPoblacion += total
			key = str(params[2])+str(params[3])+str(params[4])
			evaluados[key] = total
			
			if(total<mejor):
				mejor = total
				inidividuoMejor = [params[2],params[3],params[4]]
				if(total<TotalGlobal):
					mejorGlobal = [params[2],params[3],params[4]]
					TotalGlobal = mejor
					EnergiaGlobal = float(result)
					DistanciaGlobal = distancia

        val_corr = str(params[2]) + "," + str(params[3]) + "," + str(params[4])        
				

		
	#print("#### FInaliza paralelismo ####")
	#print(evaluados)
	evalPoblacion = evalPoblacion/tamPoblacion
	return [evalPoblacion,inidividuoMejor,mejor]

###Main
mejorGlobal = None
TotalGlobal = 1000000000
EnergiaGlobal = 1000000000
DistanciaGlobal = 1000000000

allergenName = "allergen_autopsf"
igeName = "ige_autopsf"
route = "./getEnergyAutomatedPython/merge_files/"

parser = ArgumentParser()
parser.add_argument("-c", "--core", dest="num_cores", help="numero de hilos usados para procesar", metavar="NUMERO", default=1)
parser.add_argument("-n", "--name", dest="test_name", help="nombre del directorio de prueba", metavar="DIRECTORIO")
parser.add_argument("-e", "--epitope", dest="epitope_opc", help="configuracion de epitope", metavar="OPCION",default =0)
parser.add_argument("-p", "--paratope", dest="paratope_opc", help="configuracion de paratope", metavar="OPCION", default =1)
parser.add_argument("-t", "--tamanio", dest="tamanio_poblacion", help="elmentos de la poblacion", metavar="NUMERO", default =8)
parser.add_argument("-ep", "--epocas", dest="epocas_opc", help="numero de epocas", metavar="NUMERO", default =100)

args = parser.parse_args()

#print(args)

nameTest = str(args.test_name)
num_cores = int(args.num_cores)
epitope_opc = int(args.epitope_opc)
paratope_opc = int(args.paratope_opc)
tamanio_poblacion = int(args.tamanio_poblacion)
epocas_opc = int(args.epocas_opc)
testPATH = "ResultsAG"

if(nameTest == "error"):
    print("Debe ingresar un nombre de test")
    exit(0)

#print("Ingrese el nombre de la prueba :")
#nameTest = str(raw_input())

crearDirectorios(nameTest,testPATH)

writeLog(nameTest,"Directorio prueba : "+ str(nameTest),testPATH)

structure = PDBParser().get_structure(igeName, route + "PDBS/" +igeName+".pdb")  #Abre el archivo.
[x_cIge,y_cIge,z_cIge,distanciaMaxIge,posIge] = obtenerCentro(structure)

structure2 = PDBParser().get_structure(allergenName, route + "PDBS/" + allergenName+".pdb") #Abre el archivo.
[x_cAl,y_cAl,z_cAl,distanciaMaxAl,posAl] = obtenerCentro(structure2)

print([distanciaMaxAl,posAl,distanciaMaxIge,posIge])

igeAtoms = obtenerParatopos.obtenerParatopos(nameTest,paratope_opc,testPATH)
igeAtoms = recorrerAtoms(igeAtoms,-x_cIge,-y_cIge,-z_cIge)
#print(len(igeAtoms))

allergenAtoms = obtenerEpitopes.obtenerEpitopes(nameTest,epitope_opc,testPATH)
allergenAtoms = recorrerAtoms(allergenAtoms,-x_cAl,-y_cAl,-z_cAl)
#print(len(allergenAtoms))


radio = (distanciaMaxAl+distanciaMaxIge)*1.5

writeLog(nameTest,"Tam. Radio : "+str(radio),testPATH)

##parametrizar variable
tamPoblacion = tamanio_poblacion
writeLog(nameTest,"Tam. Poblacion : "+str(tamPoblacion),testPATH)

poblacionInicial = inicializarPoblacion(tamPoblacion,radio)
#print(poblacionInicial)

valMejor = 1000000000
mejor = None
cambio = 0
epocas = epocas_opc
epocasCambio = 15

evaluados = {}

writeLog(nameTest,"Epocas totales: "+str(epocas),testPATH)

writeLog(nameTest,"Inicio general",testPATH)
writeLog(nameTest,str(datetime.datetime.now()),testPATH)

for x in range(0,epocas):    
    print('\nEpoca: '+ str(x+1))
    writeLog(nameTest,"Epoca "+str(x+1)+ " de "+ str(epocas),testPATH)
    updateEpoca(nameTest,str(x+1))

    evaluada = evaluacionPoblacion(poblacionInicial,allergenAtoms,igeAtoms,igeName,allergenName,x,nameTest,testPATH)
    print(evaluada)    

    interMejor = evaluada[1]
    JuntarEstructuras.interPDB(igeName,allergenName,float(interMejor[0]),float(interMejor[1]),float(interMejor[2]),"Inter"+str(x),nameTest,testPATH)
    if(evaluada[2]<valMejor):							##Se reinicia la poblacion despues de n epocas con un mejor estancado.
        valMejor = evaluada[2]
        mejor = evaluada[1]
        cambio = 0
    cambio += 1
    
    if(cambio<=epocasCambio):
        #print('\n\tinicial: '+str(poblacionInicial)+"\t"+str(len(poblacionInicial)))
        writeLog(nameTest,'\tinicial: '+str(poblacionInicial)+"\t"+str(len(poblacionInicial)),testPATH)
        poblacionDominante = seleccionTorneo(poblacionInicial,allergenAtoms,igeAtoms,igeName,allergenName)
        #print('\n\tdominantes: '+str(poblacionDominante)+"\t"+str(len(poblacionDominante)))
        writeLog(nameTest,'\tdominantes: '+str(poblacionDominante)+"\t"+str(len(poblacionDominante)),testPATH)

        poblacionFinal = cruzaPoblacion(poblacionDominante)
        #print('\n\tcruza: '+str(poblacionFinal)+"\t"+str(len(poblacionFinal)))
        writeLog(nameTest,'\tcruza: '+str(poblacionFinal)+"\t"+str(len(poblacionFinal)),testPATH)

        poblacionFinal = insertarMejor(poblacionFinal,evaluada[1])
        #print('\n\tInsertado: '+str(poblacionFinal)+"\t"+str(len(poblacionFinal)))
        writeLog(nameTest,'\tInsertado: '+str(poblacionFinal)+"\t"+str(len(poblacionFinal)),testPATH)

        poblacionInicial = poblacionFinal
        #print('\n\tfinal: '+str(poblacionInicial)+"\t"+str(len(poblacionInicial)))
        writeLog(nameTest,'\tfinal: '+str(poblacionInicial)+"\t"+str(len(poblacionInicial)),testPATH)

    else:												##reinicio
        poblacionInicial = inicializarPoblacion(tamPoblacion,radio)
        cambio=0
        writeLog(nameTest,'\tcambio de poblacion---------------------------------',testPATH)
		
    writeLog(nameTest,"Epoca : "+str(x)+" Coordenada : "+str(mejorGlobal)+"distancia : "+str(DistanciaGlobal)+" energia: "+str(EnergiaGlobal)+" = "+str(TotalGlobal),testPATH)
    updateAGScores(nameTest,str(DistanciaGlobal),str(EnergiaGlobal),str(TotalGlobal),str(mejorGlobal))

print([mejor,valMejor])
writeLog(nameTest,'Valor mejor :' + str([mejor,valMejor]),testPATH)
finishTask(nameTest)

filenameBest = "bestAcoplamiento"+nameTest 
JuntarEstructuras.interPDB(igeName,allergenName,float(mejor[0]),float(mejor[1]),float(mejor[2]),filenameBest,nameTest,testPATH)

writeLog(nameTest,"Fin general",testPATH)
writeLog(nameTest,str(datetime.datetime.now()),testPath)



