#!/bin/bash
source /home/customer/anaconda3/etc/profile.d/conda.sh
conda activate tt

T1="true"
T2=$6

if [ "$T2" = "$T1" ]; then
  echo "con rb "
  python -W ignore minimaxEnergyCompleto.py -n $1 -e $2 -p $3 -d $4 -s $5 -xb $7 -yb $8 -zb $9 	
else
  echo "sin rb"
	python -W ignore minimaxEnergyCompleto.py -n $1 -e $2 -p $3 -d $4 -s $5 -rb
fi